class AccountResponse {
  String id;
  String accountId;
  String email;
  String firstName;
  String lastName;
  String pictureUrl;
  String nationality;

  AccountResponse({this.id, this.accountId, this.email, this.firstName, this.lastName, this.nationality, this.pictureUrl});

  factory AccountResponse.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return AccountResponse(
      id: json['id'] as String,
      accountId: json['accountId'] as String,
      email: json['email'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      pictureUrl: json['pictureUrl'] as String,
      nationality: json['nationality'] as String,
    );
  }
}