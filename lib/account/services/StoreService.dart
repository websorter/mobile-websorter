import 'package:shared_preferences/shared_preferences.dart';



class StoreService {
  static StoreService storeService;
  final SharedPreferences preferences;

  StoreService(this.preferences);


  static StoreService getInstance() {
    return storeService;
  }

  void set(String key, String value) async  {
    preferences.setString(key, value);
  }

  String get(String key) {
    return preferences.getString(key);
  }
}