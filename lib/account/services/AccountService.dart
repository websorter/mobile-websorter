
import 'dart:convert';
import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:websorter_mobile/account/models/AccountResponse.dart';
import 'package:websorter_mobile/account/models/GoogleIdToken.dart';

import 'StoreService.dart';

final baseUrl = DotEnv().env['API_BASE_URL'];


class AccountService {
  static final uri = baseUrl + '/account';
  static final headers = {HttpHeaders.contentTypeHeader : 'application/json; charset=utf-8',
                          HttpHeaders.acceptHeader : 'application/json'};

  static Future<http.Response> login(String idToken) async {
    var url = uri + '/login';
    final googleIdToken = new GoogleIdToken(idToken);
    return http.post(url, body: googleIdToken.toJson(), headers: headers);
  }

  static Future<AccountResponse> me() async {
    var url = uri + '/me';
    final bearerToken = StoreService.storeService.get('authorization');
    headers.addAll({HttpHeaders.authorizationHeader: bearerToken});
    final response = await http.get(url, headers: headers);
    Map<String, dynamic> account = jsonDecode(response.body);
    return AccountResponse.fromJson(account);
  }
}