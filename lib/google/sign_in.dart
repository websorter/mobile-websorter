import 'dart:io';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../account/services/AccountService.dart';


final clientId = DotEnv().env['CLIENT_ID'];

final GoogleSignIn googleSignIn = GoogleSignIn(clientId: clientId);

Future<String> signInWithGoogle() async {
  try {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

    final response = await  AccountService.login(googleSignInAuthentication.idToken);
    final bearerToken = response.headers[HttpHeaders.authorizationHeader];

    return bearerToken;
  } on Exception catch (_) {
    print('Login ERROR');
    print(_.toString());
    return null;
  }

}

void signOutGoogle() async {
  await googleSignIn.signOut();

  print("User Sign Out");
}