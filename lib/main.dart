import 'package:flutter/material.dart';
import 'package:websorter_mobile/account/services/StoreService.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'LoginView.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

//void main() => runApp(MyApp());

Future main() async {
  await DotEnv().load('.env');
  final SharedPreferences preferences = await SharedPreferences.getInstance();
  StoreService.storeService = new StoreService(preferences);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WebSorter Login',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}

