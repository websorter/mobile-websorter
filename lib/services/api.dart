import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:websorter_mobile/account/models/GoogleIdToken.dart';

final baseUrl = DotEnv().env['API_BASE_URL'];

class Api {
  static final uri = baseUrl + '/hello';

  static final headers = {HttpHeaders.contentTypeHeader : 'application/json; charset=utf-8'};

  static Future<String> helloWorld() async {

    try {
      final http.Response response = await http.get( uri);

      if (response.statusCode != 200) {
        throw Error();
      }

      return response.body;

    } on SocketException {
      print('SocketException');
    } catch (error) {
      print('Another error');
    }
  }

  static Future<http.Response> login(String idToken) async {
    var url = baseUrl + '/account/login';
    final googleIdToken = new GoogleIdToken(idToken);
    return http.post(url, body: googleIdToken.toJson(), headers: headers);
  }

}