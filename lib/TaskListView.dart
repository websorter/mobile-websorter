import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:websorter_mobile/account/models/AccountResponse.dart';
import 'package:websorter_mobile/account/services/AccountService.dart';
import 'package:websorter_mobile/account/services/StoreService.dart';
import 'package:websorter_mobile/widgets/taskRowWidget.dart';
import 'package:websorter_mobile/google/sign_in.dart';
import 'package:websorter_mobile/task/TaskService.dart';
import 'package:websorter_mobile/task/models/Task.dart';
import 'package:flag/flag.dart';
import 'package:websorter_mobile/validators/UrlValidator.dart';

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {

  List<Task> tasks = [];
  bool _isLoading = false;
  Error _error;
  String  newTaskName;
  String newTaskURL;
  Timer timer;
  final _formKey = GlobalKey<FormState>();
  AccountResponse currentUser;

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    print("calling fetchTasks");
    fetchTasks();
    AccountService.me().then((value) {
      StoreService.storeService.set("accountId", value.accountId);
      currentUser = value;
    });
    timer = Timer.periodic(Duration(seconds: 15), (Timer t) {
      setState(() {
        fetchTasks();
        _buildList();
      });
    });
  }

  Future<void> fetchTasks() async {
    tasks = [];
    print("fetching tasks...");
    TaskService.all().then((List<Task> new_tasks) {
      setState(() {
        _isLoading = false;
        tasks = new_tasks;
        _error = null;
      });
    }).catchError((onError) {
      print('Lost connection to API : $onError');
    });
  }

  void _showLostConnectionToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Updating..'),
      ),
    );
  }

  Widget _buildList() {
    return tasks.length > 0
        ? RefreshIndicator(
      child: ListView.builder(
                itemCount: tasks.length,
                itemBuilder: (BuildContext context, int index) {
                  return TaskRowWidget(tasks[index], this.fetchTasks, this._buildList);
                },
              ),
        onRefresh: fetchTasks,
      )
        : Center(child: CircularProgressIndicator());
  }

  getLastAndFirstName() {
    if (currentUser!=null) {
      if (currentUser.lastName != null && currentUser.firstName != null) {
        return currentUser.lastName + ' ' + currentUser.firstName;
      }
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text('Liste des taches'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.power_settings_new),
            tooltip: 'Log Out',
            onPressed: ()
              async  {
                await signOutGoogle();
                Navigator.pop(context);
              },
          ),
        ],
      ),
      body: Container(child: _buildList(),),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return Dialog(
                  shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)), //this right here
                  child: Container(
                    height: 250,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget> [
                                TextFormField(
                                validator: Validator.validateTaskName,
                                autofocus: true,
                                decoration: new InputDecoration(
                                  labelText: 'Nom de la tâche'
                                ),
                                onChanged: (value) {newTaskName = value;},
                              ),
                              TextFormField(
                                validator: Validator.validateUrl,
                                autofocus: true,
                                decoration: new InputDecoration(
                                    labelText: 'URL',
                                    hintText: 'format : https//www.google.com/'
                                ),
                                onChanged: (value) {newTaskURL = value;},
                              )])),
                              SizedBox(
                                width: 320.0,
                                child: RaisedButton(
                                  onPressed: () {
                                    if(_formKey.currentState.validate()) {
                                      TaskService.create(newTaskName, newTaskURL).then((value) {
                                        switch(value) {
                                          case 0: {
                                            print("tasks successfully created");
                                            fetchTasks();_buildList();
                                            Navigator.pop(context, true);
                                            newTaskURL = null;
                                            newTaskName = null;
                                          }
                                          break;
                                          case 1: {
                                            print("one of the fields is empty");
                                          }
                                          break;
                                          case 2: {
                                            print("invalid url");
                                          }
                                          break;
                                          case 3: {
                                            print("bad status code from API");
                                            //_showLostConnectionToast(context);
                                          }
                                          break;
                                          default: {
                                            print("invalid code error");
                                          }
                                        }
                                      }).catchError((onError) {
                                        Scaffold.of(context)
                                            .showSnackBar(SnackBar(content: Text('Connexion perdue')));
                                      });
                                    }
                                    },
                                  child: Text(
                                    "Créer",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  color: const Color(0xFF1BC0C5),
                                ),
                              )
                        ],
                      ),
                    ),
                  ),
                );
              });
        },
        child: Icon(Icons.add),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: CircleAvatar(
                radius: 18,
                child: ClipOval(
                  child: Image.network(currentUser!=null?currentUser.pictureUrl:'https://i.stack.imgur.com/34AD2.jpg'),
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.blue
              ),
            ),
            ListTile(
              title: Text(getLastAndFirstName()),
            ),
            ListTile(
              title: Text(currentUser!=null?currentUser.email:''),
            ),
            ListTile(
              title: Flag(currentUser!=null?currentUser.nationality:'fr', height: 50, width: 50, fit: BoxFit.scaleDown),
            )
          ],
        )
      ),
    );
  }
}

