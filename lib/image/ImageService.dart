import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;



final baseUrl = DotEnv().env['API_BASE_URL'];

class ImageService {
  static final uri = baseUrl + '/image';

  static final headers = { HttpHeaders.acceptHeader : 'image/png'};

  static Future<http.Response> downloadImageFromUrl(String url) async {
    var requestUrL = uri + '?url=' + url;
    return http.get(requestUrL, headers: headers);
  }




}