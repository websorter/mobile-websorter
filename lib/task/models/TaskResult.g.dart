// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TaskResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskResult _$TaskResultFromJson(Map<String, dynamic> json) {
  return TaskResult(
    success: json['success'] as bool,
    type: DocumentTypeHelper.fromValue(json['type'] as String).name,
    subject: json['subject'] as String,
    error: json['error'] as String,
    progression: json['progression'] as int,
  );
}

Map<String, dynamic> _$TaskResultToJson(TaskResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'type': instance.type,
      'subject': instance.subject,
      'error': instance.error,
      'progression': instance.progression,
    };
