import 'package:json_annotation/json_annotation.dart';
import 'package:websorter_mobile/task/models/document.type.dart';
part 'TaskResult.g.dart';

@JsonSerializable()
class TaskResult {
  bool success;
  String type;
  String subject;
  String error;
  int progression;

  TaskResult({this.success, this.type, this.subject, this.error, this.progression});

  factory TaskResult.fromJson(Map<String, dynamic> json) {
    if(json == null) return null;
    return TaskResult(
      success: json['success'] as bool,
      type: DocumentTypeHelper.fromValue( json['type'] as String).name,
      subject: json['subject'] as String,
      error: json['error'] as String,
      progression: json['progression'] as int
    );
  }
  Map<String, dynamic> toJson() => _$TaskResultToJson(this);
}