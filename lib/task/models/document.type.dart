
enum DocumentType {
  UnidentifiedPage,
  NotFoundPage, // valeur lorsque le runner ne trouve pas la page
  WelcomePage, // page d’accueil
  SignUpPage, // page d'inscription
  LoginPage, // Page de connexion
  ProfilePage, // Page de profil
  ECommercePage, // Page e-commerce
  ArticlesPage, // Page liste d’article
  StreamPage, // Page de streaming
  InformationPage, // Page d’information
  ContactPage, // Page de contact
  LegalDisclaimerPage, //Page de mention légale
  FormPage, // Page de formulaire
  PaymentPage // Page de paiement

}


extension DocumentTypeExtension on DocumentType {
  String get value {
    switch (this) {
      case DocumentType.UnidentifiedPage:
        return 'Failed to identify';
      case DocumentType.NotFoundPage:
        return 'NotFound Page';
      case DocumentType.WelcomePage:
        return 'Welcome Page';
      case DocumentType.SignUpPage:
        return 'SignUp Page';
      case DocumentType.LoginPage:
        return 'Login Page';
      case DocumentType.ProfilePage:
        return 'Profile Page';
      case DocumentType.ECommercePage:
        return 'E-Commerce Page';
      case DocumentType.ArticlesPage:
        return 'Articles Page';
      case DocumentType.StreamPage:
        return 'Stream Page';
      case DocumentType.InformationPage:
        return 'Information Page';
      case DocumentType.ContactPage:
        return 'Contact Page';
      case DocumentType.LegalDisclaimerPage:
        return 'Legal Disclaimer Page';
      case DocumentType.FormPage:
        return 'Form Page';
      case DocumentType.PaymentPage:
        return 'Payment Page';
    }
  }

  String get name {
    switch (this) {
      case DocumentType.UnidentifiedPage:
        return 'Failed to identify';
      case DocumentType.NotFoundPage:
        return 'Page non trouvé';
      case DocumentType.WelcomePage:
        return 'Page d\'accueil';
      case DocumentType.SignUpPage:
        return 'Page d\'inscription';
      case DocumentType.LoginPage:
        return 'Page d\'authentification';
      case DocumentType.ProfilePage:
        return 'Page de profile';
      case DocumentType.ECommercePage:
        return 'Page de E-commerce';
      case DocumentType.ArticlesPage:
        return 'Page d\'articles';
      case DocumentType.StreamPage:
        return 'Page de streaming';
      case DocumentType.InformationPage:
        return 'Page d\'information';
      case DocumentType.ContactPage:
        return 'Page de contact';
      case DocumentType.LegalDisclaimerPage:
        return 'Page de mention légal';
      case DocumentType.PaymentPage:
        return 'Page de paiement';
      case DocumentType.FormPage:
        return 'Page de formulaire';
        break;
    }
  }
}


class DocumentTypeHelper {
  static DocumentType fromValue(String value) {
    switch (value) {
      case 'Failed to identify':
        return DocumentType.UnidentifiedPage;
      case 'NotFound Page':
        return DocumentType.NotFoundPage;
      case 'Welcome Page':
        return DocumentType.WelcomePage;
      case 'SignUp Page':
        return DocumentType.SignUpPage;
      case 'Login Page':
        return DocumentType.LoginPage;
      case 'Profile Page':
        return DocumentType.ProfilePage;
      case 'E-Commerce Page':
        return DocumentType.ECommercePage;
      case 'Articles Page':
        return DocumentType.ArticlesPage;
      case 'Stream Page':
        return DocumentType.StreamPage;
      case 'Information Page' :
        return DocumentType.InformationPage;
      case 'Contact Page':
        return DocumentType.ContactPage;
      case 'Legal Disclaimer Page':
        return DocumentType.LegalDisclaimerPage;
      case 'Form Page':
        return DocumentType.FormPage;
      case 'Payment Page':
        return DocumentType.PaymentPage;
      default :
        return null;
    }
  }
}
