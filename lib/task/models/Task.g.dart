// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Task _$TaskFromJson(Map<String, dynamic> json) {
  return Task(
    id: json['id'] as String,
    name: json['name'] as String,
    url: json['url'] as String,
    createdDate: json['createdDate'] == null
        ? null
        : DateTime.parse(json['createdDate'] as String),
    executionDate: json['executionDate'] == null
        ? null
        : DateTime.parse(json['executionDate'] as String),
    result: json['result'] == null
        ? null
        : TaskResult.fromJson(json['result'] as Map<String, dynamic>),
    accountId: json['accountId'] as String,
  );
}

Map<String, dynamic> _$TaskToJson(Task instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'url': instance.url,
      'createdDate': instance.createdDate?.toIso8601String(),
      'executionDate': instance.executionDate?.toIso8601String(),
      'result': instance.result,
      'accountId': instance.accountId,
    };
