import 'package:json_annotation/json_annotation.dart';
import 'TaskResult.dart';

part 'Task.g.dart';

@JsonSerializable()
class Task {
  String id;
  String name;
  String url;
  DateTime createdDate;
  DateTime executionDate;
  TaskResult result;
  String accountId;

  Task({this.id, this.name, this.url, this.createdDate, this.executionDate,
       this.result, this.accountId});

  factory Task.fromJson(Map<String, dynamic> json) {
    final dateFormat = DateTime.parse(json['createdDate']);
    return Task(
      id: json['id'] as String,
      name: json['name'] as String,
      url: json['url'] as String,
      createdDate: json['createdDate'] != null ? DateTime.parse(json['createdDate']): null,
      executionDate: json['executionDate'] != null ? DateTime.parse(json['executionDate']): null,
      result: TaskResult.fromJson(json['result']),
      accountId:json['accountId'] as String
    );
  }

  Map<String, dynamic> toJson() => _$TaskToJson(this);


}