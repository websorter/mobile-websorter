import 'dart:convert';
import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:websorter_mobile/account/services/StoreService.dart';

import 'models/Task.dart';



final baseUrl = DotEnv().env['API_BASE_URL'];

class TaskService {
  static final uri = baseUrl + '/task';
  static final headers = {HttpHeaders.contentTypeHeader : 'application/json; charset=utf-8',
                          HttpHeaders.acceptHeader : 'application/json; charset=utf-8'};


  static Future<List<Task>> all() async {
    var url = uri + '/me';
    final bearerToken = StoreService.storeService.get('authorization');
    headers.addAll({HttpHeaders.authorizationHeader: bearerToken});
    final response =  await http.get(url, headers: headers).timeout(Duration(seconds: 3));
    final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
    return parsed.map<Task>((json) => Task.fromJson(json)).toList();
  }

  static Future<int> create(String newTaskName, String newTaskURL) async {
    if(newTaskName == null || newTaskURL == null) {
      return 1;
    }
    if(!new RegExp(r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)").hasMatch(newTaskURL)) {
      return 2;
    }
    final bearerToken = StoreService.storeService.get('authorization');
    headers.addAll({HttpHeaders.authorizationHeader: bearerToken});
    //print(StoreService.storeService.get("accountId"));
    Task task = new Task(name: newTaskName, url: newTaskURL, accountId: StoreService.storeService.get("accountId"));
    final response = await http.post(uri, headers: headers, body: jsonEncode(task));
    if(response.statusCode != 200) {
      print(response.body);
      return 3;
    }
    return 0;
  }

  static Future<Task> update(Task task) async {
    final response = await http.put('${uri}/${task.id}', headers: headers, body: jsonEncode(task));
    if(response.statusCode != 200) {
      print(response.body);
      return null;
    }
    try{
      final parsed = jsonDecode(response.body);
      return Task.fromJson(parsed);
    }catch(error) {
      print('error: ${error}');
      return null;
    }
  }

  static Future<bool> delete(String taskId) async {
    final bearerToken = StoreService.storeService.get('authorization');
    headers.addAll({HttpHeaders.authorizationHeader: bearerToken});
    final response = await http.delete(uri + '/' + taskId, headers: headers);
    if(response.statusCode != 200) {
      print(response.body);
      return false;
    }
    return true;
  }

}