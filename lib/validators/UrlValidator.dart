


class Validator {
  static RegExp pattern = new RegExp(r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)");

  static String validateTaskName(String value) {
      if (value.isEmpty) {
        return 'Le nom est requis';
      }
      return null;
  }

  static String validateUrl(String value) {

    if( !pattern.hasMatch(value) ) {
      return 'URL invalide';
    } else if (value.isEmpty) {
      return 'URL requise';
    }
    return null;
  }
}