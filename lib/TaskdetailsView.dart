import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:websorter_mobile/task/TaskService.dart';
import 'package:websorter_mobile/task/models/Task.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:websorter_mobile/task/models/TaskResult.dart';
import 'package:websorter_mobile/task/models/document.type.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:websorter_mobile/widgets/ImagePageWidget.dart';

final baseUrl = DotEnv().env['API_BASE_URL'];


class TaskDetailsView extends StatefulWidget {

  Task task;
  bool confirm = false;
  ImageProvider image = AssetImage("assets/websorter-logo.png");

  TaskDetailsView(this.task);

  @override
  TaskDetailsViewState createState() => TaskDetailsViewState();
}

_launchURL(url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

class TaskDetailsViewState extends State<TaskDetailsView> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text('Recherche : ${widget.task.name}'),
      ),
      backgroundColor: getBackgroundColor(widget.task),
      body: Container(
        child: Column(
          children: [
            Card(
              margin: EdgeInsets.all(10.0),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  children: <Widget>[
                    Text(
                        '${widget.task.name}',
                      style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: [
                        Spacer(),
                        InkWell(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return new ImagePageWidget('${baseUrl}/image?url=${widget.task.url}');
                                }
                            );
                          },
                          child: Container(
                            child: new Image.network('${baseUrl}/image?url=${widget.task.url}', height: 200, width: 200),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black, width: 3)
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                    Card(margin: EdgeInsets.all(10.0), child: Padding(
                      padding: const EdgeInsets.only(left: 50.0, right: 50.0, top: 8.0, bottom: 8.0),
                      child: InkWell(
                          child: Text('${widget.task.url.length > 70? 'Aller a la page' : widget.task.url }', style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline)),
                      onTap: () => {_launchURL(widget.task.url)},),
                    )),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(widget.task.executionDate!=null?'Lancement le : ${widget.task.executionDate}':''),
                        Text('Créer le : ${widget.task.createdDate}'),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
            TaskResultWidget(task: widget.task),
            Spacer(),
          ],
        ),
        alignment: Alignment.center,
      ),
    );
  }

  getBackgroundColor(task) {
    if (task.result.subject == null) {
      return Colors.lightBlueAccent;
    } else if(task.result.success) {
      return Colors.lightGreenAccent;
    } else {
      return Colors.redAccent;
    }
  }
}

class TaskResultWidget extends StatelessWidget {

  final Task task;
  TaskResultWidget({this.task});

  @override
  Widget build(BuildContext context) {
    if (this.task.result.subject == null) {
      return Card(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Résultats', style: TextStyle(fontWeight: FontWeight.bold),),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TaskStatus(
                      result: task.result,
                    ),
                    Text('Etat de la recherche : Non défini' ),
                  ]
              )
            ]
          ),
        ),
      );
    } else if(this.task.result.success) {
      return Card(
        margin: EdgeInsets.all(10.0),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
              children: [
                Text('Résultats', style: TextStyle(fontWeight: FontWeight.bold)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row (
                        children: [
                          Text('Document trouvé : ${task.result.type}  '),
                          Material(
                            child: Center(
                              child: Ink(
                                color: Colors.white,
                                width: 40.0,
                                height: 30.0,
                                child: Ink(
                                  decoration: const ShapeDecoration(
                                    color: Colors.lightBlue,
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: const Icon(Icons.create, size: 15.0,),
                                    color: Colors.white,
                                    onPressed: () {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return new DocumentTypeSelectorWidget(task);
                                          }
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ]
                    ),
                    Row (
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(' Etat de la recherche : Succès' ),
                          Spacer(),
                          TaskStatus(
                            result: task.result,
                          ),
                        ]
                    ),
                    Text('Sujet : ${task.result.subject}')
                  ],
                )
                //Text('recherche complétion : ${task.result.progression} %'),
              ]
          ),
        ),
      );
    } else {
      return Card(
        margin: EdgeInsets.all(10.0),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
              children: [
                Text('Résultats', style: TextStyle(fontWeight: FontWeight.bold),),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row (
                        children: [
                          Text('Document trouvé : ${task.result.type == null ? DocumentType.UnidentifiedPage.name : task.result.type}'),
                          Material(
                            child: Center(
                              child: Ink(
                                color: Colors.white,
                                width: 40.0,
                                height: 30.0,
                                child: Ink(
                                  decoration: const ShapeDecoration(
                                    color: Colors.lightBlue,
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: const Icon(Icons.create, size: 15.0,),
                                    color: Colors.white,
                                    onPressed: () {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return new DocumentTypeSelectorWidget(task);
                                          }
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ]
                    ),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text('Etat de la recherche : Echec' ),
                          Spacer(),
                          TaskStatus(
                            result: task.result,
                          ),
                        ]
                    ),
                    Text('Error : ${task.result.error}'),
                  ],
                )
            ]
          ),
        ),
      );
    }
  }
}


class DocumentTypeSelectorWidget extends StatefulWidget {

  DocumentType documentType = DocumentType.UnidentifiedPage;
  Task task;

  DocumentTypeSelectorWidget(this.task);

  @override
  DocumentTypeSelectorWidgetState createState() => DocumentTypeSelectorWidgetState();
}

class DocumentTypeSelectorWidgetState extends State<DocumentTypeSelectorWidget> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
        shape: RoundedRectangleBorder( borderRadius:  BorderRadius.circular(20.0)),
        child: Container(
          height: 350,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                Text("Selectionnez le type de document"),
                Spacer(),
                ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButton<DocumentType>(
                      hint: Text("Select document type"),
                      value: widget.documentType,
                      icon: Icon(Icons.arrow_downward),
                      items: DocumentType.values.map( (DocumentType eachDocumentType) {
                        return DropdownMenuItem<DocumentType>(
                          value: eachDocumentType,
                          child: Text('${eachDocumentType.name}'),
                        );
                      }).toList(),
                      onChanged: (DocumentType value) {
                        setState(() {
                          widget.documentType = value;
                        });
                      },
                      isExpanded: true,
                    ),
                ),

                Spacer(),
                Row(
                  children: [
                    Spacer(),
                    RaisedButton(
                        onPressed: () async {
                          widget.task.result.type = DocumentTypeHelper.fromValue( widget.documentType.value ).value;
                          Task taskUpdated = await TaskService.update(widget.task);
                          setState(() {
                            widget.task = taskUpdated;
                          });
                          Navigator.pop(context);
                        },
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color(0xFF0D47A1),
                                Color(0xFF1976D2),
                                Color(0xFF42A5F5),
                              ],
                            ),
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child: const Text('Modifier', style: TextStyle(fontSize: 20)),
                        )
                    ),
                    Spacer(),
                    RaisedButton(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          decoration: const BoxDecoration(
                            color: Colors.grey,
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child: const Text('Cancel', style: TextStyle(fontSize: 20)),
                        )
                    ),
                    Spacer(),
                  ],
                )
            ],
          ),
        ),
      )
    );
  }

}


class TaskStatus extends StatelessWidget {
  final TaskResult result;

  TaskStatus({this.result});

  @override
  Widget build(BuildContext context) {
    if(this.result.subject == null) {

      return Ink(
        decoration: const ShapeDecoration(
          color: Colors.lightBlueAccent,
          shape: CircleBorder(),
        ),
        child: Icon(
          Icons.timelapse,
          color: Colors.white,
        ),
      );
    } else if (this.result.success) {
      return Ink(
        decoration: const ShapeDecoration(
          color: Colors.lightGreenAccent,
          shape: CircleBorder(),
        ),
        child: Icon(
          Icons.check,
          color: Colors.white,
        ),
      );
    }
    return Ink(
      decoration: const ShapeDecoration(
        color: Colors.redAccent,
        shape: CircleBorder(),
      ),
      child: Icon(
        Icons.cancel,
        color: Colors.white,
      ),
    );
  }
}
