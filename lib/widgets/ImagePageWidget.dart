
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:websorter_mobile/task/models/Task.dart';

class ImagePageWidget extends StatefulWidget {
  String url;

  ImagePageWidget(this.url);

  @override
  ImagePageWidgetState createState() => ImagePageWidgetState();
}

class ImagePageWidgetState extends State<ImagePageWidget> {
  @override
  Widget build(BuildContext context) {
   return Dialog(
     shape: RoundedRectangleBorder(
         borderRadius:
         BorderRadius.circular(20.0)), //this right here
     child: Container(
       height: 550,
       width: 450,
       child: Padding(
         padding: const EdgeInsets.all(1.0),
         child: Column(
           mainAxisAlignment: MainAxisAlignment.center,
           crossAxisAlignment: CrossAxisAlignment.start,
           children: [
             Image.network(widget.url, height: 500, width: 350)
           ],
         ),
       ),
     ),
   );
  }
}