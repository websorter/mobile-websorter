import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:websorter_mobile/task/TaskService.dart';
import 'package:websorter_mobile/task/models/Task.dart';
import 'package:websorter_mobile/task/models/TaskResult.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:websorter_mobile/TaskdetailsView.dart';
import 'package:websorter_mobile/task/models/document.type.dart';


final baseUrl = DotEnv().env['API_BASE_URL'];


typedef RefreshCallBack = void Function();


class TaskRowWidget extends StatefulWidget {
  Task task;
  bool confirm = false;

  final RefreshCallBack refreshCallBack;
  final RefreshCallBack buildCallBack;

  TaskRowWidget(this.task, this.refreshCallBack, this.buildCallBack);

  @override
  State<StatefulWidget> createState() => TaskRowWidgetState();

}

class TaskRowWidgetState extends State<TaskRowWidget> {
  @override
  Widget build(BuildContext context) {
    if(widget.task == null) {
      return ListTile(
        title: Text("No task"),
      );
    }
    return Dismissible(
      key: Key(widget.task.id),
      onDismissed: (direction) {
        print("task " + widget.task.name + " dismissed");
      },
      direction: DismissDirection.endToStart,
      background: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        color: Colors.red,
        alignment: Alignment.centerRight,
        child: Icon(Icons.delete),
      ),
      confirmDismiss: (DismissDirection direction) async {
        return await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Confirmation"),
              content: const Text("Êtes-vous sûr de vouloir supprimer cette tâche ?"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => {
                      setState(() {
                        TaskService.delete(widget.task.id).then((value) {
                          if(value) {
                            print('Successfully deleted task ' + widget.task.name);
                            widget.refreshCallBack();
                            widget.buildCallBack();
                          } else {
                            print('Failed to delete task ' + widget.task.name);
                          }
                        });
                        Navigator.of(context).pop(true);
                      })
                    },
                    child: const Text("Supprimer")
                ),
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: const Text("Annuler"),
                ),
              ],
            );
          },
        );
      },
      child: ListTile(
        isThreeLine: true,
        leading:  TaskStatus (
          result: widget.task.result,
        ),

        title: Text(widget.task.name),

        subtitle: Column(
          children: <Widget>[
            Text('Résultat : ${widget.task.result.type==null? DocumentType.UnidentifiedPage.name :widget.task.result.type}'),
            Text('Nom de la page : ${widget.task.result.subject==null?"":widget.task.result.subject}')
          ],
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.announcement,
            color: Colors.amber,
          ),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) {
                  return TaskDetailsView(widget.task);
                },
              ),
            );
          },
        ),
      ),
    );
  }
}





class TaskStatus extends StatelessWidget {
  final TaskResult result;

  TaskStatus({this.result});

  @override
  Widget build(BuildContext context) {
    if(this.result.subject == null) {
      return Icon(Icons.timelapse,
        color: Colors.blue,
      );
    } else if (this.result.success) {
      return Icon(
        Icons.check,
        color: Colors.green,
      );
    }
    return Icon(
      Icons.cancel,
      color: Colors.red,
    );
  }
}

String dateFormat(DateTime dateTime) {
  try{
    return DateFormat('dd/MM/yyyy kk:mm:ss').format(dateTime);

  }catch(error) {
    return '';
  }
}
